///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//    Counts down (or towards) a significant date
//
// Example:
//    $ ./countdown
//    Reference time: Tue Feb 22 04:30:00 PM HST 2022
//    Years: 0 Days: 0 Hours: 4 Minutes: 33 Seconds: 0
//    Years: 0 Days: 0 Hours: 4 Minutes: 32 Seconds: 59
//    Years: 0 Days: 0 Hours: 4 Minutes: 32 Seconds: 58
//    Years: 0 Days: 0 Hours: 4 Minutes: 32 Seconds: 57
//
// Compilation:
//    make
//
// @file countdown.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   22 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>

int main() {
   struct tm refTime;
   time_t referenceTime, seconds, sDiff;

   //refTime = Tue Feb 22 04:30:00 PM HST 2022
   refTime.tm_mon = 2 - 1;                      //month, 0 to 11
   refTime.tm_mday = 22;                        //day, 1 to 31
   refTime.tm_year = 2022 - 1900;               //years since 1900
   refTime.tm_hour = 16;                        //hour, 0 to 32
   refTime.tm_min = 30;                         //min, 0 to 59
   refTime.tm_sec = 0;                          //sec, 0 to 59

   referenceTime = mktime(&refTime);
   
   printf("Reference time: Tue Feb 22 04:30:00 PM HST 2022\n");

   while (true) {
      sleep(1);
      seconds = time(NULL);
      sDiff = difftime(referenceTime, seconds);
      printf("Years: %ld ", sDiff / 31536000);
      printf("Days: %ld ", (sDiff%31536000) / 86400);
      printf("Hours: %ld ", (sDiff%31536000%86400) / 3600);
      printf("Minutes: %ld ", (sDiff%31536000%86400%3600) / 60);
      printf("Seconds: %ld\n", sDiff%31536000%86400%3600%60);
   }   
   return 0;
}
